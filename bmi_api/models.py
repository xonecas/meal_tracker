from datetime import date
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class UserPreferences(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='preferences')
    max_calories = models.IntegerField(default=2000)


class Meal(models.Model):
    owner = models.ForeignKey('auth.User', related_name='meals', on_delete=models.CASCADE)
    date = models.DateField("Meal date", default=date.today)
    time = models.TimeField("Meal time", default=(lambda: timezone.now().time()))
    text = models.TextField("Meal description or title")
    calories = models.IntegerField()

    def __str__(self):
        return self.text
