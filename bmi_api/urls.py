from django.conf.urls import url, include
from rest_framework import routers
from .views import UserViewSet, MealViewSet, login

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'meals', MealViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'login', login),
]
