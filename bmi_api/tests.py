import json
from datetime import datetime, timedelta

from django.test import TestCase, Client
from django.urls import reverse

from rest_framework import status

from .models import Meal
from .serializers import MealSerializer

client = Client()


class MealListTest(TestCase):

    def setUp(self):
        eight_hours_later = datetime.now() + timedelta(hours=8)
        Meal.objects.create(
            text='Lunch time', calories='200')
        Meal.objects.create(
            text='Dinner time', calories='180',
            date=eight_hours_later, time=eight_hours_later.time())

    def test_get_all_meals(self):
        response = client.get(reverse('meals-list'))
        serializer = MealSerializer(Meal.objects.all(), many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_new_meal(self):
        yesterday_lunch = datetime.now() - timedelta(hours=24)
        response = client.post(
            reverse('meals-list'),
            data=json.dumps({
                'text': 'Quick small lunch',
                'calories': 190,
                'date': yesterday_lunch.__str__(),
                'time': yesterday_lunch.time().__str__()
            }),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class MealDetailTest(TestCase):

    def setUp(self):
        self.meal = Meal.objects.create(
            text='Lunch time', calories='200')

    def test_get_meal_by_pk(self):
        response = client.get(reverse('meals-detail', kwargs={'pk': self.meal.pk}))
        serializer = MealSerializer(Meal.objects.get(pk=self.meal.pk))

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_edit_meal(self):
        yesterday_lunch = datetime.now() - timedelta(hours=24)
        response = client.put(
            reverse('meals-detail', kwargs={'pk': self.meal.pk}),
            data=json.dumps({
                'text': 'small lunch',
                'calories': 110,
                'date': yesterday_lunch.__str__(),
                'time': yesterday_lunch.time().__str__()
            }),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_meal(self):
        response = client.delete(reverse('meals-detail', kwargs={'pk': self.meal.pk}))

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
