from django.contrib import admin
from .models import UserPreferences, Meal


admin.site.register(UserPreferences)
admin.site.register(Meal)
