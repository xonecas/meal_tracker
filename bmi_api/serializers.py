from django.contrib.auth.models import User

from rest_framework import serializers

from .models import Meal, UserPreferences


class UserPreferencesSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserPreferences
        fields = ['user', 'max_calories']
        read_only_fields = ['user']


class UserSerializer(serializers.ModelSerializer):
    preferences = UserPreferencesSerializer()

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'preferences']
        read_only_fields = ['id']
        extra_kwargs = {'password': {'write_only': True}}
        depth = 2

    def create(self, validated_data):
        preferences_data = validated_data.pop('preferences')
        password = validated_data.pop('password')

        user = User.objects.create(**validated_data)
        user.set_password(password)
        user.save()

        UserPreferences.objects.create(user=user, **preferences_data)

        return user

    def update(self, user, validated_data):
        preferences_data = validated_data.pop('preferences')
        preferences, created = UserPreferences.objects.get_or_create(user=user)

        user.username = validated_data.get('username', user.username)
        user.save()

        if (created):
            preferences.max_calories = preferences_data.get('max_calories')
        else:
            preferences.max_calories = preferences_data.get(
                'max_calories', preferences.max_calories
            )
        preferences.save()

        return user


class MealSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Meal
        fields = '__all__'
        extra_kwargs = {
            'date': {'required': False},
            'time': {'required': False},
        }
