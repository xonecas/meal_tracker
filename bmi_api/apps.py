from django.apps import AppConfig


class BmiApiConfig(AppConfig):
    name = 'bmi_api'
