from datetime import datetime

from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.utils.dateparse import parse_time

from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.authtoken.models import Token

from .models import Meal
from .permissions import IsOwnerOrReadOnly
from .serializers import MealSerializer, UserSerializer


@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")

    if username is None or password is None:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    user = authenticate(username=username, password=password)

    if not user:
        return Response(status=status.HTTP_404_NOT_FOUND)

    serializer = UserSerializer(user)
    token, _ = Token.objects.get_or_create(user=user)

    return Response({'token': token.key, **serializer.data}, status=status.HTTP_200_OK)


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [IsOwnerOrReadOnly]
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        permission_classes = self.permission_classes
        if self.action == 'create':
            permission_classes = [permissions.AllowAny]

        return [permission() for permission in permission_classes]


class MealViewSet(viewsets.ModelViewSet):
    queryset = Meal.objects.all()
    permission_classes = [IsOwnerOrReadOnly]
    serializer_class = MealSerializer

    def get_queryset(self):
        if self.request.user.is_anonymous:
            return Meal.objects.none()

        queryset = Meal.objects.filter(owner=self.request.user)

        query_params = self.request.query_params

        from_date = query_params.get('date_from', None)
        to_date = query_params.get('date_to', None)
        from_time = query_params.get('time_from', None)
        to_time = query_params.get('time_to', None)

        if from_date is not None and to_date is not None:
            from_date = datetime.date(datetime.strptime(from_date, '%Y-%m-%d'))
            to_date = datetime.date(datetime.strptime(to_date, '%Y-%m-%d'))
            queryset = queryset.filter(date__gte=from_date, date__lte=to_date)

        if from_time is not None and to_time is not None:
            from_time = parse_time(from_time)
            to_time = parse_time(to_time)
            queryset = queryset.filter(time__gte=from_time, time__lte=to_time)

        return queryset.order_by('time', 'date')

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
