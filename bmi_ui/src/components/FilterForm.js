import React from 'react'
import PropTypes from 'prop-types'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap'
import axios from 'axios'
import { API_URL } from '../constants'

class UserForm extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      date_from: '',
      date_to: '',
      time_from: '',
      time_to: ''
    }
  }

  static get propTypes () {
    return {
      user: PropTypes.any,
      toggle: PropTypes.func,
      update: PropTypes.func
    }
  }

  onChange (e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  filter (e) {
    e && e.preventDefault()

    const params = {}

    if (this.state.date_from !== '') params.date_from = this.state.date_from
    if (this.state.date_to !== '') params.date_to = this.state.date_to
    if (this.state.time_from !== '') params.time_from = this.state.time_from
    if (this.state.time_to !== '') params.time_to = this.state.time_to

    axios.get(API_URL + 'meals', {
      params: params,
      crossdomain: true,
      headers: { Authorization: 'Token ' + this.props.user.token }
    }).then((response) => {
      this.props.toggle()
      this.props.update(response.data)
    })
  };

  render () {
    const boundChange = this.onChange.bind(this)

    return (
      <Form onSubmit={this.filter.bind(this)}>
        <FormGroup>
          <Label for="date_from">Start date:</Label>
          <Input
            type="date"
            name="date_from"
            onChange={boundChange}
            value={this.state.date_from}
          />
        </FormGroup>
        <FormGroup>
          <Label for="date_to">End date:</Label>
          <Input
            type="date"
            name="date_to"
            onChange={boundChange}
            value={this.state.date_to}
          />
        </FormGroup>
        <FormGroup>
          <Label for="date_from">Start time:</Label>
          <Input
            type="time"
            name="time_from"
            onChange={boundChange}
            value={this.state.time_from}
          />
        </FormGroup>
        <FormGroup>
          <Label for="date_from">End time:</Label>
          <Input
            type="time"
            name="time_to"
            onChange={boundChange}
            value={this.state.time_to}
          />
        </FormGroup>

        <Button className="float-right" color="primary">Filter</Button>
      </Form>
    )
  }
}

export default UserForm
