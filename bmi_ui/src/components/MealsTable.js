import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table, Button } from 'reactstrap'
import MealModal from './MealModal'
import axios from 'axios'
import { API_URL } from '../constants'

class MealsTable extends Component {
  static get propTypes () {
    return {
      create: PropTypes.boolean,
      meals: PropTypes.any,
      user: PropTypes.any,
      toggle: PropTypes.func,
      update: PropTypes.func
    }
  }

  delete (e, value) {
    e && e.preventDefault()

    const mealId = e.currentTarget.getAttribute('data-meal-id')

    axios.delete(API_URL + 'meals/' + mealId + '/', {
      crossdomain: true,
      headers: { Authorization: 'Token ' + this.props.user.token }
    }).then((response) => {
      this.props.update()
    })
  }

  render () {
    const mealsGroupedByDay = {}

    this.props.meals.forEach((m, i) => {
      mealsGroupedByDay[m.date] = mealsGroupedByDay[m.date] || 0
      mealsGroupedByDay[m.date] += m.calories
    })

    this.props.meals.forEach((m) => {
      if (mealsGroupedByDay[m.date] > this.props.user.preferences.max_calories) {
        m.color = 'pt-3 text-danger'
      } else {
        m.color = 'pt-3 text-success'
      }
    })

    return (
      <Table>
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Calories</th>
            <th>Date</th>
            <th>Time</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {this.props.meals.map((m, i) =>
            <tr key={i}>
              <th scope='row' className='pt-3'>{i + 1}</th>
              <th className={m.color}>{m.text}</th>
              <th className={m.color}>{m.calories}</th>
              <th className={m.color}>{m.date}</th>
              <th className={m.color}>{m.time}</th>
              <th className='no-pad-right'>
                <MealModal
                  meal={m}
                  user={this.props.user}
                  update={this.props.update}
                />
                <Button
                  color='danger'
                  outline
                  data-meal-id={m.id}
                  onClick={this.delete.bind(this)}
                  size='sm'
                  className='mr-2 float-right'
                >Delete</Button>
              </th>
            </tr>
          )}
        </tbody>
      </Table>
    )
  }
}

export default MealsTable
