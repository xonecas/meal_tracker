import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap'
import LoginForm from './LoginForm'

class UserModal extends Component {
  constructor (props) {
    super(props)

    this.state = {
      modal: false
    }
  }

  static get propTypes () {
    return {
      update: PropTypes.func
    }
  }

  toggle (e) {
    e && e.preventDefault()

    this.setState(previous => ({
      modal: !previous.modal
    }))
  }

  render () {
    const boundToggle = this.toggle.bind(this)

    return (
      <Fragment>
        <Button onClick={boundToggle} color='primary' size='sm'>Login</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={boundToggle}>Login</ModalHeader>

          <ModalBody>
            <LoginForm
              toggle={boundToggle}
              update={this.props.update}
            />
          </ModalBody>
        </Modal>
      </Fragment>
    )
  }
}

export default UserModal
