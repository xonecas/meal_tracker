import React from 'react'
import PropTypes from 'prop-types'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap'
import axios from 'axios'
import { API_URL } from '../constants'

class MealForm extends React.Component {
  constructor (props) {
    super(props)

    const meal = props.meal || {}

    this.state = {
      id: meal.id,
      text: meal.text || '',
      calories: meal.calories || 0,
      date: meal.date,
      time: meal.time
    }
  }

  static get propTypes () {
    return {
      meal: PropTypes.any,
      user: PropTypes.any,
      toggle: PropTypes.func,
      update: PropTypes.func
    }
  }

  onChange (e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  create (e) {
    e.preventDefault()

    axios.post(API_URL + 'meals/', this.state, {
      crossdomain: true,
      headers: { Authorization: 'Token ' + this.props.user.token }
    }).then((response) => {
      this.props.toggle()
      this.props.update()
    })
  }

  edit (e) {
    e.preventDefault()

    if (this.state.date === '') delete this.state.date
    if (this.state.time === '') delete this.state.time

    axios.put(API_URL + 'meals/' + this.state.id + '/', this.state, {
      crossdomain: true,
      headers: { Authorization: 'Token ' + this.props.user.token }
    }).then((response) => {
      this.props.toggle()
      this.props.update()
    })
  }

  render () {
    const boundChange = this.onChange.bind(this)

    return (
      <Form onSubmit={this.props.meal ? this.edit.bind(this) : this.create.bind(this)}>
        <FormGroup>
          <Label for='text'>Title:</Label>
          <Input
            type='text'
            name='text'
            onChange={boundChange}
            value={this.state.text}
          />
        </FormGroup>
        <FormGroup>
          <Label for='text'>Calories:</Label>
          <Input
            type='text'
            name='calories'
            onChange={boundChange}
            value={this.state.calories}
          />
        </FormGroup>
        <FormGroup>
          <Label for='text'>Date (empty for today):</Label>
          <Input
            type='date'
            name='date'
            onChange={boundChange}
            value={this.state.date}
          />
        </FormGroup>
        <FormGroup>
          <Label for='time'>Time (empty for right now):</Label>
          <Input
            type='time'
            name='time'
            onChange={boundChange}
            value={this.state.time}
          />
        </FormGroup>

        <Button className='float-right' color='primary'>Save</Button>
      </Form>
    )
  }
}

export default MealForm
