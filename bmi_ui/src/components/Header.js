import React from 'react'
import PropTypes from 'prop-types'
import { Nav, Navbar, NavbarBrand, NavItem, NavbarText } from 'reactstrap'
import UserModal from './UserModal'
import LoginModal from './LoginModal'

const Header = (props) => {
  let login = ''
  if (!props.user) {
    login = (
      <NavItem>
        <LoginModal user={props.user} update={props.update} />
      </NavItem>
    )
  } else {
    login = (
      <NavItem>
        <NavbarText className="text-light p-1">{props.user.username}</NavbarText>
      </NavItem>
    )
  }

  return (
    <Navbar color="dark" dark expand="md">
      <NavbarBrand href="/">Meal Tracker</NavbarBrand>
      <Nav className="ml-auto" navbar>
        {login}
        <NavItem className="ml-1">
          <UserModal create={!props.user} user={props.user} update={props.update} />
        </NavItem>
      </Nav>
    </Navbar>
  )
}

Header.propTypes = {
  update: PropTypes.func,
  user: PropTypes.any
}

export default Header
