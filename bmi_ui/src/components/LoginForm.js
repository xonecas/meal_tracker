import React from 'react'
import PropTypes from 'prop-types'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap'
import axios from 'axios'
import { API_URL } from '../constants'

class UserForm extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      username: '',
      password: ''
    }
  }

  static get propTypes () {
    return {
      user: PropTypes.any,
      toggle: PropTypes.func,
      update: PropTypes.func
    }
  }

  onChange (e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  login (e) {
    e && e.preventDefault()

    axios.post(API_URL + 'login/', this.state, { crossdomain: true }).then((response) => {
      this.props.toggle()
      this.props.update(response.data)
    })
  }

  render () {
    const boundChange = this.onChange.bind(this)

    return (
      <Form onSubmit={this.login.bind(this)}>
        <FormGroup>
          <Label for="username">Username:</Label>
          <Input
            type="text"
            name="username"
            onChange={boundChange}
            value={this.state.name}
          />
        </FormGroup>
        <FormGroup>
          <Label for="examplePassword">Password</Label>
          <Input
            type="password"
            name="password"
            onChange={boundChange}
            value={this.state.name}
          />
        </FormGroup>

        <Button className="float-right" color="primary">Login</Button>
      </Form>
    )
  }
}

export default UserForm
