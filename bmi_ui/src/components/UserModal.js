import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap'
import UserForm from './UserForm'

class UserModal extends Component {
  constructor (props) {
    super(props)

    this.state = {
      modal: false
    }
  }

  static get propTypes () {
    return {
      user: PropTypes.any,
      create: PropTypes.boolean,
      update: PropTypes.func
    }
  }

  toggle (e) {
    e && e.preventDefault()

    this.setState(previous => ({
      modal: !previous.modal
    }))
  }

  render () {
    const create = this.props.create
    const toggle = this.toggle.bind(this)

    let title = 'Editing user'
    let button = <Button onClick={toggle} size='sm' outline>Edit user</Button>

    if (create) {
      title = 'Creating user'

      button = (
        <Button
          color='primary'
          size='sm'
          outline
          onClick={toggle}
        >Signup</Button>
      )
    }

    return (
      <Fragment>
        {button}
        <Modal isOpen={this.state.modal} toggle={toggle}>
          <ModalHeader toggle={toggle}>{title}</ModalHeader>

          <ModalBody>
            <UserForm
              toggle={toggle}
              user={this.props.user}
              update={this.props.update}
            />
          </ModalBody>
        </Modal>
      </Fragment>
    )
  }
}

export default UserModal
