import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap'
import FilterForm from './FilterForm'

class FilterModal extends Component {
  constructor (props) {
    super(props)

    this.state = {
      modal: false
    }
  }

  static get propTypes () {
    return {
      user: PropTypes.any,
      toggle: PropTypes.func,
      update: PropTypes.func
    }
  }

  toggle (e) {
    e && e.preventDefault()

    this.setState(previous => ({
      modal: !previous.modal
    }))
  };

  render () {
    const boundToggle = this.toggle.bind(this)

    return (
      <Fragment>
        <Button
          className="mr-3"
          onClick={boundToggle}
          size="sm"
          outline
          color="primary">
          Filter
        </Button>
        <Modal isOpen={this.state.modal} toggle={boundToggle}>
          <ModalHeader toggle={boundToggle}>Filter</ModalHeader>
          <ModalBody>
            <FilterForm
              toggle={boundToggle}
              user={this.props.user}
              update={this.props.update}
            />
          </ModalBody>
        </Modal>
      </Fragment>
    )
  }
}

export default FilterModal
