import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap'
import MealForm from './MealForm'

class MealModal extends Component {
  constructor (props) {
    super(props)

    this.state = {
      modal: false
    }
  }

  static get propTypes () {
    return {
      create: PropTypes.boolean,
      meal: PropTypes.any,
      user: PropTypes.any,
      update: PropTypes.func
    }
  }

  toggle (e) {
    e && e.preventDefault()

    this.setState(previous => ({
      modal: !previous.modal
    }))
  }

  render () {
    const create = this.props.create
    const toggle = this.toggle.bind(this)

    let title = 'Editing meal'
    let button = (
      <Button onClick={toggle} size='sm' outline className='mr-3 float-right'>Edit</Button>
    )

    if (create) {
      title = 'Creating meal'

      button = (
        <Button
          color='primary'
          size='sm'
          outline
          onClick={toggle}
          className='mr-3 float-right'
        >Add new meal</Button>
      )
    }

    return (
      <Fragment>
        {button}
        <Modal isOpen={this.state.modal} toggle={toggle}>
          <ModalHeader toggle={toggle}>{title}</ModalHeader>

          <ModalBody>
            <MealForm
              toggle={toggle}
              meal={this.props.meal}
              user={this.props.user}
              update={this.props.update}
            />
          </ModalBody>
        </Modal>
      </Fragment>
    )
  }
}

export default MealModal
