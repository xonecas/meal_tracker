import React, { Fragment, Component } from 'react'
import axios from 'axios'
import { API_URL } from '../constants'
import Header from './Header'
import MealsTable from './MealsTable'
import MealModal from './MealModal'
import FilterModal from './FilterModal'
import { Jumbotron, Col, Container, Row } from 'reactstrap'

class Home extends Component {
  constructor (props) {
    super(props)

    this.state = {
      user: false,
      meals: []
    }
  }

  setUser (u) {
    this.setState({ user: u })
    this.getMeals()
  }

  setMeals (m) {
    this.setState({ meals: m })
  }

  getMeals () {
    axios.get(
      API_URL + 'meals',
      { crossdomain: true, headers: { Authorization: 'Token ' + this.state.user.token } }
    ).then(res => {
      this.setState({
        meals: res.data
      })
    })
  }

  render () {
    let container = ''

    if (this.state.user) {
      container = (
        <Container style={{ marginTop: '20px' }}>
          <Row>
            <Col>
              <div className="mb-3 float-right">
                <FilterModal
                  user={this.state.user}
                  update={this.setMeals.bind(this)}
                />
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <MealsTable
                user={this.state.user}
                meals={this.state.meals}
                update={this.getMeals.bind(this)}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="float-right">
                <MealModal
                  create={true}
                  user={this.state.user}
                  update={this.getMeals.bind(this)}>
                </MealModal>
              </div>
            </Col>
          </Row>
        </Container>
      )
    } else {
      container = (
        <div>
          <Jumbotron>
            <h1 className="display-3">Meal track</h1>
            <p className="lead">Please login or create an account to start</p>
          </Jumbotron>
        </div>
      )
    }

    return (
      <Fragment>
        <Header user={this.state.user} update={this.setUser.bind(this)} />
        {container}
      </Fragment>
    )
  }
}

export default Home
