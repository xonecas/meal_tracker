import React from 'react'
import PropTypes from 'prop-types'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap'
import axios from 'axios'
import { API_URL } from '../constants'

class UserForm extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      id: this.props.user.id || '',
      username: this.props.user.username || '',
      preferences: this.props.user.preferences || { max_calories: 0 }
    }
  }

  static get propTypes () {
    return {
      user: PropTypes.any,
      toggle: PropTypes.func,
      update: PropTypes.func
    }
  }

  onChange (e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  onCaloriesChange (e) {
    this.setState({ preferences: { max_calories: e.target.value } })
  }

  createUser (e) {
    e.preventDefault()

    axios.post(API_URL + 'users/', this.state, { crossdomain: true }).then(() => {
      axios.post(API_URL + 'login/', this.state, { crossdomain: true }).then((res) => {
        this.props.toggle()
        this.props.update(res.data)
      })
    })
  }

  editUser (e) {
    e.preventDefault()

    axios.put(API_URL + 'users/' + this.state.id + '/', this.state, {
      crossdomain: true,
      headers: { Authorization: 'Token ' + this.props.user.token }
    }).then((response) => {
      const token = this.props.user.token
      response.data.token = token
      this.props.toggle()
      this.props.update(response.data)
    })
  }

  render () {
    const boundChange = this.onChange.bind(this)
    const boundCaloriesChange = this.onCaloriesChange.bind(this)

    return (
      <Form onSubmit={this.props.user ? this.editUser.bind(this) : this.createUser.bind(this)}>
        <FormGroup>
          <Label for='username'>Username:</Label>
          <Input
            type='text'
            name='username'
            onChange={boundChange}
            value={this.state.username}
          />
        </FormGroup>
        <FormGroup>
          <Label for='examplePassword'>Password:</Label>
          <Input
            type='password'
            name='password'
            onChange={boundChange}
            value={this.state.password}
          />
        </FormGroup>
        <FormGroup>
          <Label for='caloriesPerDay'>Maximum Calories per Day:</Label>
          <Input
            type='text'
            name='caloriesPerDay'
            onChange={boundCaloriesChange}
            value={this.state.preferences.max_calories}
          />
        </FormGroup>

        <Button className='float-right' color='primary'>Save</Button>
      </Form>
    )
  }
}

export default UserForm
